/****************************************
*  Author: Champion Liu (16309030)
* Project: number recognition by ANN
* Version: Beta 1.2		[2017-4-16]
*    Note: This c++ file is for
		   ann number recognition
****************************************/

#include "FOC.h"
#include "NANN.h"
#include <opencv2/ml/ml.hpp>
#include <string>

using namespace cv;
using namespace foc;
using namespace nann;
using namespace std;

int main(int argc, char** argv)
{
	cout << "/*****************************************/\n"
		 << "*	welcome to use ANN demo.          *\n" 
		 << "*	this is a console app(x64).       *\n"
		 << "*	number recognition by ANN.        *\n"
		 << "*	-- by ChampionLiu 16309030        *\n"
		 << "/*****************************************/" << endl;

	CvANN_MLP ann;

	bool annIsLoad = false;
	bool isQuit = false;
	while(!isQuit)
	{
		cout << ">>>input command: ";
		string cmd;
		cin >> cmd;

		if(cmd == "quit")
		{
			isQuit = true;
			cout << "goodbye." << endl;
		}
		else if(cmd == "help")
		{
			cout << "[helpdesk]:\n"
				 << " - quit<enter> : quit the application.\n"
				 << " - help<enter> : ask for help desk.\n"
				 << " - create<enter> : create a multi-layers perceptrons ANN.\n"
				 << " - train<enter> : train ANN machine.\n"
				 << " - predict<enter> : use trained data to predict.\n"
				 << " - load<enter> : load xml training data.\n"
				 << " - info<enter> : get training information."
				 << endl;
		}
		else if(cmd == "create")
		{
			cout << "ann\\create>" << endl;
			cout << "- mode: 1.default; 2.custom; 3.auto;" << endl;
			cout << "input mode (an integer): ";
			int mode;
			cin >> mode;

			if(mode == 1)
			{
				NANNParam params;
				ann.create(params.ntrLayers, params.activeFunc, params.fparam1, params.fparam2);
				cout << "[ann] ANN created successfully!" << endl;
			}
			else if(mode == 2)
			{
				NANNParam params;

				cout << "input the num of layers will set: ";
				int num;
				cin >> num;
				cout << "input corresponding number of each layer: ";
				Mat nLayers(1, num, CV_32SC1);
				for(int i = 0; i < num; i++)
					cin >> nLayers.at<int>(i);
				params.ntrLayers = nLayers;
				
				cout << "- method: 0.IDENTITY; 1.SIGMOID_SYM; 2.GAUSSIAN;" << endl;
				cout << "input training method: ";
				int method;
				cin >> method;
				switch(method)
				{
					case 0:
						params.activeFunc = CvANN_MLP::IDENTITY;
						break;
					case 1:
						params.activeFunc = CvANN_MLP::SIGMOID_SYM;
						break;
					case 2:
						params.activeFunc = CvANN_MLP::GAUSSIAN;
						break;
					default:
						cout << "unknown method. use default = 1." << endl;
						break;
				}

				cout << "- fparam for 1, 2 in doubles;" << endl;
				cout << "input two doubles: ";
				int f1, f2;
				cin >> f1 >> f2;
				params.fparam1 = f1;
				params.fparam2 = f2;

				ann.create(params.ntrLayers, params.activeFunc, params.fparam1, params.fparam2);
				cout << "[ann] ANN created successfully!" << endl;
			}
			else if(mode == 3)
			{
				NANNParam params;

				cout << "input the path of one of training data: ";
				string path;
				cin >> path;
				Mat img = imread(path);
				Mat nLayers(1, 3, CV_32SC1);
				if(img.data)
				{
					nLayers.at<int>(0) = img.cols;
					nLayers.at<int>(1) = img.rows;
				}
				else
				{
					cout << "read file error. use default." << endl;
					nLayers.at<int>(0) = 3;
					nLayers.at<int>(1) = 5;
				}
				cout << "- an integer (zero or negative as default)" << endl;
				cout << "intput the neutron num of output layer: ";
				int factor;
				cin >> factor;
				if(factor > 0)
					nLayers.at<int>(2) = factor;
				else
					nLayers.at<int>(2) = 10;

				params.ntrLayers = nLayers;

				ann.create(params.ntrLayers, params.activeFunc, 1, 1);
				cout << "[ann] ANN created successfully!" << endl;
			}
		}
		else if(cmd == "train")
		{
			cout << "- train modes: 0.single sample; 1.multi sample;" << endl;
			cout << "input mode: ";
			int mode;
			cin >> mode;
			if(mode == 0)
			{
				cout << "input feature class: ";
				float ft;
				cin >> ft;
				cout << "input training data: ";
				string path;
				cin >> path;
				Mat img = imread(path);
				if(img.data)
				{
					/*
					vector<float> grad;
					getGrad(img, grad);
					float conGrad[1][8];
					for(int i = 0; i < 8; i++)
					{
						conGrad[1][i] = grad.at(i);
					}

					Mat trainMat = Mat(1, img.cols, CV_32FC1, conGrad);
					*/
	
					cvtColor(img, img, CV_BGR2GRAY);
					resize(img, img, Size(8,16) );
					img.reshape(1);
					Mat trainMat;
					img.convertTo(trainMat, CV_32FC1);

					float fts[1][8 * 16];
					for(int i = 0; i < img.cols; i++)
						fts[1][i] = ft;
					Mat trainLabel = Mat(1, img.cols, CV_32FC1, fts);
					ann.train(trainMat, trainLabel, Mat() );

					cout << "train finished." << endl;
				}
				else
				{
					cout << "read files error." << endl;
				}
			}
		}
		else if(cmd == "predict")
		{
			NANNPrtAls analys;

			if(!annIsLoad)
			{
				cout << "- you have not load xml yet. you can input \"load\" to load;" << endl;
				cout << "- read the path of Matrix of features with labels files;" << endl;
				cout << "intput file name:";
				string path;
				cin >> path;
				Mat lists = imread(path);
				for(int i = 0; i < lists.rows; i++)
				{
					analys.features.push_back(lists.at<float>(0,i) );
					analys.labels.push_back(lists.at<char>(1,i) );
				}
				cout << "read in." << endl;
			}
			else
			{
				cout << "ann xml loaded." << endl;
			}

			cout << "- read in path of image that wait to predict;" << endl;
			cout << "input file name: ";
			string path;
			cin >> path;
			Mat imgSmp = imread(path);
			if(imgSmp.data)
			{
				//to do some pre-procession here:
				cvtColor(imgSmp, imgSmp, CV_RGB2GRAY);
				blur(imgSmp, imgSmp, Size(3,3) );
				threshold(imgSmp, imgSmp, 128, 255, CV_THRESH_BINARY);
				resize(imgSmp, imgSmp, Size(8,16) );
				imgSmp.reshape(1);

				//ananlyze
				analys.resultVal = ann.predict(imgSmp, analys.imgRes);

				int index;
				string label;
				if(analys.classify(index, label) )
				{
					cout << "index = " << index << endl;
					cout << "label = " << label << endl;
				}
				else
				{
					cout << "unfound." << endl;
				}
			}
			else
			{
				cout << "read image error." << endl;
			}

		}
		else if(cmd == "load")
		{
			cout << "- xml trained data file path (space as default)" << endl;;
			cout << "input file name: ";
			char* path;
			cin >> path;
			if(path == " ")
				ann.load("./dat/res.xml");
			else
				ann.load(path);
			cout << "load finished." << endl;

			annIsLoad = true;
		}
		else if(cmd == "info")
		{
			cout << "---------information-----------" << endl;

			int layerCount = ann.get_layer_count();
			cout << "> layer count: " << layerCount << endl;
			
			cout << "> layer weights:" << endl;
			for(int i = 0; i < layerCount; i++)
			{
				cout << "[index = " << i << " ] "; 
				double* w = ann.get_weights(i);
				int len = sizeof(w) / sizeof(double);
				for(int j = 0; j < len - 1; j++)
					cout << w[j] << ", "; 
				cout << w[len - 1] << endl;
			}

			cout << "-------------------------------" << endl;
		}
		else
		{
			cout << "unknown command. please input \"help\" to ask for guide." << endl;
		}

	}
}

