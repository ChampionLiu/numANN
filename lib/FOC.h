/****************************************
*  Author: Champion Liu (16309030)
* Project: number recognition by ANN
* Version: Beta 1.2		[2017-4-16]
*    Note: This head file is for
		   easy operating controller
		   from the feature of number.
****************************************/

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <iostream>
#include <string>

using namespace cv;
using namespace std;

/*
	This head file includes:
		- parameters configurature:
			> class FOCParam;
		- feature extract:
			> void setMask(_T matrix);
			> void setSize(Size s);
			> float getGraySum(Mat InputImg);
			> void getGrad(Mat InputImg, vector<float>& grad);
			> void getSeries(Mat InputImg, vector<float>& series, Size normSize = Size(8,16) );
		- feature pre-process:
			> void rotRectH(Mat InputImg, Mat& OutputImg, float v_thresh = 150, float angle_thresh = 30);
			> void rotRectC(Mat InputImg, Mat& OutputImg, const unsigned int maxLinesNum = 12, const unsigned char threshBeginVal = 60);
			

	For more help, please contact with Champion Liu 16309030
	or go to Lab C203 or D301-A in East School of SYSU, Guangzhou
	to find the Assistant of Lab whose name is "Liu Yinyi".
*/

/*feature operating controller*/
namespace foc
{
	class FOCParam
	{
	public:
		int canny_min;
		int canny_max;
		int parallelDis;
		int repDis;
		Mat maskMat;
		Size picSize;
		
		FOCParam()
		{
			canny_min = 30;
			canny_max = 90;
			parallelDis = 10;
			repDis = 10;
			maskMat = (Mat_<float>(3,3) << 1,2,1,0,0,0,-1,-2,-1 );
			picSize = Size(8,16);
			
		}
	};

	FOCParam params;	
	
	/*******************Feature Extract********************/
	
	/*set mask of sobel matrix*/
	void setMask(Mat matrix)
	{
		for(int i = 0; i < 3; i++)
			for(int j = 0; j < 3; j++)
				params.maskMat.at<float>(j,i) = matrix.at<float>(j,i);
	}

	/*set normalize size of image*/
	void setSize(Size s)
	{
		params.picSize = s;
	}

	/*get gray-scale sum of the image*/
	float getGraySum(Mat InputImg)
	{
		float sum = 0;
		int r = InputImg.rows, c = InputImg.cols;
		if(InputImg.isContinuous())
		{
			c *= r;
			r = 1;
		}

		for(int i = 0; i < r; i++)
		{
			const unsigned char* linePtr = InputImg.ptr<unsigned char>(i);
			for(int j = 0; j < c; j++)
				sum += linePtr[j];
		}

		return sum;
	}

	/*get sobel gradient */
	void getGrad(Mat InputImg, vector<float>& grad)
	{
		Mat img, sobelX, sobelY;
		cvtColor(InputImg, img, CV_BGR2GRAY);
		resize(img, img, params.picSize);

		Mat y_mask = params.maskMat.clone();
		y_mask = y_mask / 8;
		Mat x_mask = y_mask.t();
		
		filter2D(img, sobelX, CV_32F, x_mask);
		filter2D(img, sobelY, CV_32F, y_mask);

		sobelX = abs(sobelX);
		sobelY = abs(sobelY);

		float totalX = getGraySum(sobelX);
		float totalY = getGraySum(sobelY);

		//it seems that the image is divided into 4 * 2
		for(int i = 0; i < img.rows; i += 4)
			for(int j = 0; j < img.cols; j += 4)
			{
				Mat subX = sobelX(Rect(j, i, 4, 4));
				grad.push_back(getGraySum(subX) / totalX);
				Mat subY = sobelY(Rect(j, i, 4, 4));
				grad.push_back(getGraySum(subY) / totalY);
			}
	}

	/*get series feature of normalize image*/
	void getSeries(Mat InputImg, vector<float>& series, Size normSize = Size(8,16) )
	{
		Mat img;
		cvtColor(InputImg, img, CV_BGR2GRAY);
		resize(img, img, normSize);

		img.reshape(0,1);
		for(int i = 0; i < img.cols; i++)
			series.push_back(img.at<float>(i) );
	}



	/********************pre-process feature********************/

	/*houghLine rectify rotation from inclined type to horizontal type*/
	void rotRectH(Mat InputImg, Mat& OutputImg, float v_thresh = 150, float angle_thresh = 30, int HOUGH_VOTE = 100)
	{
		Mat imgOri = InputImg.clone();
		cvtColor(InputImg, InputImg, CV_BGR2GRAY);
		Mat img = Mat::zeros(InputImg.size(), CV_32F);
		Point cen(img.cols / 2, img.rows / 2);

		int opWidth = getOptimalDFTSize(InputImg.rows);
		int opHeigh = getOptimalDFTSize(InputImg.cols);

		//fill to accelerate in DFT
		copyMakeBorder(InputImg, img,
				0, opWidth - InputImg.rows,
				0, opHeigh - InputImg.cols,
				BORDER_CONSTANT, Scalar::all(0) );

		//Discrete Fourier Transform
		Mat imgMix = Mat::zeros(img.size(), CV_32FC2);
		Mat planes[] = {Mat_<float>(img), Mat::zeros(img.size(), CV_32F)};
		merge(planes, 2, imgMix);
	
		dft(imgMix, imgMix);

		//get DFT image
		split(imgMix, planes);
		magnitude(planes[0], planes[1], planes[0]); //calculate the mod of vector

		Mat imgMag = planes[0];
		imgMag += Scalar::all(1);
		log(imgMag, imgMag); //reduce brightness range

		imgMag = imgMag(Rect(0, 0, imgMag.cols & -2, imgMag.rows & -2));
		int cx = imgMag.cols / 2, cy = imgMag.rows / 2;
		
		Mat q0(imgMag, Rect(0, 0, cx, cy) );
		Mat q1(imgMag, Rect(0, cy, cx, cy) );
		Mat q2(imgMag, Rect(cx, cy, cx, cy) );
		Mat q3(imgMag, Rect(cx, 0, cx, cy) );

		Mat tmp;
		q0.copyTo(tmp);
		q2.copyTo(q0);
		tmp.copyTo(q2);

		q1.copyTo(tmp);
		q3.copyTo(q1);
		tmp.copyTo(q3);

		normalize(imgMag, imgMag, 0, 1, CV_MINMAX);
		Mat imgDFT(imgMag.size(), CV_8UC1);
		imgMag.convertTo(imgDFT, CV_8UC1, 255, 0);

		threshold(imgDFT, imgDFT, v_thresh, 255, CV_THRESH_BINARY);

		//draw hough lines
		vector<Vec2f> lines;
		float pi180 = (float)CV_PI / 180;
		Mat imgLin = Mat::zeros(imgDFT.size(), CV_8UC3);
		HoughLines(imgDFT, lines, 1, pi180, HOUGH_VOTE, 0, 0);
		for(int i = 0; i < lines.size(); i++)
		{
			float rho = lines[i][0], theta = lines[i][1];
			Point pt1, pt2;
			double a = cos(theta), b = sin(theta);
			double x0 = a*rho, y0 = b*rho;
			pt1.x = cvRound(x0 - 1000*b);
			pt1.y = cvRound(y0 + 1000*a);
			pt2.x = cvRound(x0 + 1000*b);
			pt2.y = cvRound(y0 - 1000*a);
			line(imgLin, pt1, pt2, Scalar(255,255,255), 3, 8, 0);
		}

		//calculate the inclined angle
		float angle = 0;
		const float piThresh = (float)CV_PI / 90;
		const float piHalf = CV_PI / 2;
		for(int i = 0; i < lines.size(); i++)
		{
			float theta = lines[i][1];
			if(abs(theta) < piThresh || abs(theta - piHalf) < piThresh)
				continue;
			else
			{
				angle = theta;
				break;
			}
		}

		angle = (angle < piHalf) ? angle : (angle - CV_PI);
		if(angle != piHalf)
		{
			float angleT = img.rows * tan(angle) / img.cols;
			angle = atan(angleT);
		}
		float angleD = angle * 180 / (float)CV_PI;

		//get correctly rotated image
		Mat matRot = getRotationMatrix2D(cen, angleD, 1.0);
		Mat imgDst = Mat::ones(imgOri.size(), CV_8UC3);
		warpAffine(imgOri, imgDst, matRot, imgOri.size(), 1, 0, Scalar(255,255,255) );
		OutputImg = imgDst;
	}

	/*findContour rectify rotation from inclined type to horizontal type*/
	void rotRectC(Mat InputImg, Mat& OutputImg, const unsigned int maxLinesNum = 12)
	{
		int cth = params.canny_min;
		vector<Vec4i> lines;
		Mat img;
		cvtColor(InputImg, img, CV_RGB2GRAY);
		do
		{
			//lines.clear();
			Canny(img, img, cth, params.canny_max);
			threshold(img, img, 128, 255, CV_THRESH_BINARY);
			HoughLinesP(img, lines, 1, CV_PI / 180, 50, 100, 100);
			cth += 2; 
		}while(lines.size() > maxLinesNum && cth < params.canny_max);

		//if getting a negative num means pass this step
		if(params.parallelDis > 0)
			for(int i = 0; i < lines.size(); i++)
			{
				if(abs(lines[i][0] - lines[i][2]) < params.parallelDis || abs(lines[i][1] - lines[i][3]) < params.parallelDis)
				{
					lines.erase(lines.begin() + i);
				}
			}

		vector<Point2f> points(lines.size() * 2);
		for(int i = 0; i < lines.size(); i++)
		{
			points[i * 2].x = lines[i][0];
			points[i * 2].y = lines[i][1];
			points[i * 2 + 1].x = lines[i][2];
			points[i * 2 + 1].y = lines[i][3];
		}

		//erase the repeated point
		vector<Point2f> filterP(points);
		for(int i = 0; i < points.size() - 1; i++)
			for(int j = i + 1; j < filterP.size(); j++)
				if(abs(points[i].x - filterP[j].x) < params.repDis &&
				   abs(points[i].y - filterP[j].y) < params.repDis &&
				   abs(points[i].x - filterP[j].x) > 0 &&
				   abs(points[i].y - filterP[j].y) > 0)
					filterP.erase(filterP.begin() + i);

		//sort in rank
		for(int i = 0; i < filterP.size() - 1; i++)
			for( int j = filterP.size(); j > i; j--)
				if( (pow(filterP[j - 1].x, 2) + pow(filterP[j - 1].y, 2) ) > (pow(filterP[j].x, 2) +pow(filterP[j].y, 2) ) )
				{
					Point pt;
					pt = filterP[j - 1];
					filterP[j - 1] = filterP[j];
					filterP[j] = pt;
				}

		//find isClockWise rotate
		float tgTheta = (float)img.rows / img.cols;
		float tmp = tgTheta * filterP[0].x;
		tmp = (float)filterP[0].y - tmp;
		bool isClockWise = (tmp >= 0) ? true : false;

		//find border points
		int indexLD = 0, indexRT = 0;
		float ptDisMaxLD = 0, ptDisMaxRT = 0;
		Point2f pt_leftTop, pt_rightDown, pt_leftDown, pt_rightTop;
		vector<float> ptDisList;
		pt_leftTop = filterP[0];
		pt_rightDown = filterP[filterP.size() - 1];
		
		ptDisList.push_back(0);
		for(int i = 1; i < filterP.size() - 1; i++)
		{
			ptDisList.push_back(pow(filterP[i].x - pt_leftTop.x, 2) + 
								pow(filterP[i].y - pt_leftTop.y, 2) +
								pow(filterP[i].x - pt_rightDown.x, 2) + 
								pow(filterP[i].y - pt_rightDown.y, 2) );
		}
		ptDisList.push_back(0);

		for(int i = 1; i < filterP.size() - 1; i++)
		{
			float parDomain = (float)filterP[i].y - tgTheta * filterP[i].x;
			if(parDomain > 0)
			{
				if(ptDisList[i] > ptDisMaxLD)
				{
					ptDisMaxLD = ptDisList[i];
					indexLD = i;
				}
			}
			else
			{
				if(ptDisList[i] > ptDisMaxRT)
				{
					ptDisMaxRT = ptDisList[i];
					indexRT = i;
				}
			}
		}

		pt_leftDown = filterP[indexLD];
		pt_rightTop = filterP[indexRT];

		//transform
		vector<Point2f> ptsHmo, ptsDst;
		ptsHmo.push_back(pt_leftTop);
		ptsHmo.push_back(pt_rightTop);
		ptsHmo.push_back(pt_rightDown);
		ptsHmo.push_back(pt_leftDown);

		ptsDst.push_back(Point(0, 0) );
		ptsDst.push_back(Point(img.cols - 1, 0) );
		ptsDst.push_back(Point(img.cols - 1, img.rows - 1) );
		ptsDst.push_back(Point(0, img.rows - 1) );

		Mat hmo = findHomography(ptsHmo, ptsDst);
		warpPerspective(InputImg, OutputImg, hmo, InputImg.size() );
	}
};
