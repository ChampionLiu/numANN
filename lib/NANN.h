/****************************************
*  Author: Champion Liu (16309030)
* Project: number recognition by ANN
* Version: Beta 1.2		[2017-4-16]
*    Note: This head file is for
		   easy operating controller
		   from the feature of number.
****************************************/

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/ml/ml.hpp>
#include <vector>
#include <string>

using namespace cv;
using namespace std;

namespace nann
{
	class NANNParam
	{
	public:
		Mat ntrLayers;
		Mat trainData;
		int activeFunc;
		double
			fparam1,
			fparam2;

		NANNParam()
		{
			ntrLayers = (Mat_<int>(1,5) << 3,5,4,4,3);
			activeFunc = CvANN_MLP::SIGMOID_SYM;
			fparam1 = fparam2 = 0;
		}
	};

	class NANNPredictAnalysis
	{
	public:
		vector<float> features;
		vector<char> labels;
		Mat imgRes;
		float resultVal;

		NANNPredictAnalysis()
		{
			features.clear();
			labels.clear();
			resultVal = 0;
		}

		bool classify(int& index, string& label)
		{
			if(features.size() != labels.size() )
				return false;
			for(int i = 0; i < features.size(); i++)
			{
				if(resultVal != features[i])
					continue;
				else
				{
					index = i;
					label = labels[i];
				}
			}
		}
	};

	typedef NANNPredictAnalysis NANNPrtAls;
};
